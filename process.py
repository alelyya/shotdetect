import pygame, sys
from audio import AudioPlayer, SignalType, Task


def process(objects, recorder, signal):
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            for obj in objects:
                obj.die()
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                buzzer = AudioPlayer(Task.PLAY, SignalType.BUZZER, 'buzzer.wav')
                buzzer.start()
    keys = pygame.key.get_pressed()
    
    if keys[pygame.K_1]:
        recorder.frames_handler.change_view(1)
    if keys[pygame.K_2]:
        recorder.frames_handler.change_view(2)
    if keys[pygame.K_3]:
        recorder.frames_handler.change_view(3)
    if keys[pygame.K_RIGHT]:
        signal.frames_handler.clean_screen = True
        signal.frames_handler.global_offset+=10
    if keys[pygame.K_LEFT]:
        signal.frames_handler.clean_screen = True
        signal.frames_handler.global_offset-=10
    if keys[pygame.K_RETURN]:
        if not signal.started:
            signal.start()
