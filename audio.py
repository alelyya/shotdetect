from handler import FramesHandler
from threading import Thread

import pyaudio, wave
import numpy as np
import time, random


class AudioIO(Thread):

    def __init__(self, task, *args):
        Thread.__init__(self)
        
        self.task = task
        
        self.CHUNK = 512
        self.framerate = 22000

        if task == Task.PLAY:
            self.signal_type, self.file, self.frames = args
            self.set_player()
        elif task == Task.RECORD:
            self.set_recorder()
    
    def set_player(self):

        self.started = False

        if self.frames:
            self.frames_handler = FramesHandler(self.CHUNK, self.framerate, self.frames)
            return
        
        self.p_signal = pyaudio.PyAudio()
        self.signal = wave.open(self.file, 'rb')
        self.nchannels, self.sampwidth, self.framerate, self.nframes, self.comptype, self.compname = self.signal.getparams()
        
        types = {1: np.int8, 2: np.int16, 3: np.int32}
        data = self.signal.readframes(self.nframes)
        frames = np.fromstring(data, dtype=types[self.sampwidth])
        self.frames_handler = FramesHandler(self.CHUNK, self.framerate, frames)
        
    def set_recorder(self):
        
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 1
        self.RATE = 22000

        self.recording = True
        self.event = True
        self.frames_handler = FramesHandler(self.CHUNK, self.RATE)
        self.frames_handler.sample_freq = np.fft.rfftfreq(self.CHUNK, 1./self.RATE)


class AudioPlayer(AudioIO):
    
    def __init__(self, task, signal_type, file, frames = []):
        AudioIO.__init__(self, task, signal_type, file, frames)
        
    def run(self):
        
        if self.signal_type == SignalType.BUZZER:
            time.sleep(random.randint(1, 3))
            self.time_history = []

        self.started = True
        self.start_time = time.time()

        self.signal_stream = self.p_signal.open(format = self.p_signal.get_format_from_width(self.signal.getsampwidth()),
                                                channels=self.signal.getnchannels(), rate = self.signal.getframerate(),
                                                output = True)
        
        data = self.frames_handler.get_frames()
        self.signal_stream.write(np.ndarray.tostring(data))

        self.signal_stream.stop_stream()
        self.signal_stream.close()

    def record(self, time):
        
        self.time_history.append(time)
        for elem in self.time_history:
            print(elem - self.start_time, end=' ')

    def draw(self, screen):

        self.frames_handler.draw_all(screen)

    def die(self):
        
        self.p_signal.terminate()


class AudioRecorder(AudioIO):
    
    def __init__(self, task):
        AudioIO.__init__(self, task)

    def run(self):

        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format = self.FORMAT, channels = self.CHANNELS, rate = self.RATE,
                                  input=True, frames_per_buffer = self.CHUNK)

        while self.recording:
            self.record()

    def record(self):

        data = np.fromstring(self.stream.read(self.CHUNK), np.int16)
        self.frames_handler.record(data)

    def draw(self, screen):
        
        self.frames_handler.draw_last(screen)

    def die(self):
        self.recording = False
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()


class Task():
    RECORD = 0
    PLAY = 1


class SignalType():
    BUZZER = 0
    SHOT = 1
    RELOAD = 2
    TONE = 3
    RANDOM = 4
