import pygame
from process import process
from draw import draw
import numpy as np
from audio import AudioRecorder, AudioPlayer, SignalType, Task
from classifier import Classifier


class Main():

    def __init__(self, SCREENWIDTH, SCREENHEIGHT, basePath):

        self.screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
        self.objects = []

        self.classifier = Classifier()
        self.recorder = AudioRecorder(Task.RECORD)
        
        self.signal = AudioPlayer(Task.PLAY, SignalType.RELOAD, 'test/test_1.wav')
        
        self.classifier.classify(self.signal)
        #self.classifier.dtw_test()

        self.objects.append(self.signal)
        #self.objects.append(self.recorder)

        self.main()

    def main(self):
        
        pygame.init()
        self.recorder.start()
        
        self.screen.fill([255,255,255])
        
        while True:
            
            event = self.recorder.frames_handler.event_recorder.new_events()
            if event:
                np_event = np.ndarray.tostring(np.asarray(event))
                np_event_signal = AudioPlayer(Task.PLAY, SignalType.RANDOM, '', np_event)
                self.classifier.classify(np_event_signal)

            process(self.objects, self.recorder, self.signal)
            draw(self.screen, self.objects)
