import pygame


def draw(screen, objects):
    
    for obj in objects:
        obj.draw(screen)

    pygame.display.flip()
