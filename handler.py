import numpy as np
import audioop
import pygame

from python_speech_features import mfcc


class EventRecorder():
    
    def __init__(self):

        self.frame_counter=1000

        self.event_frames_list = []
        self.event_amp_list = []
        self.event_fft_data_list = []
        
        self.event_frames = []
        self.event_amp = []
        self.event_fft_data = []

        self.event_found = False
        self.ITL = 1000
        self.ITU = 5*self.ITL

    def event_check(self, data, amplitude, fft_data):
        
        if amplitude > self.ITU:
            self.event_found = True

        if amplitude > self.ITL:
            self.event_frames.append(data)
            self.event_amp.append(amplitude)
            self.event_fft_data.append(fft_data)

        else:
            if self.event_found:
                self.record_event()
            else:
                self.event_frames = []
                self.event_amp = []
                self.event_fft_data = []

    def record_event(self):

        self.event_frames_list.append(self.event_frames)
        self.event_amp_list.append(self.event_amp)
        self.event_fft_data_list.append(self.event_fft_data)
        self.event_found = False
        self.event_amp = []
        self.event_fft_data = []

    def new_events(self):
        event = []
        if self.event_frames_list:
            event = self.event_frames_list[-1]
            self.event_frames_list = []
        return event


class FramesHandler():
    
    def __init__(self, CHUNK, RATE, frames=[0]):

        self.CHUNK = CHUNK
        self.RATE = RATE

        self.event_recorder = EventRecorder()

        self.frames = frames
        self.amp_frames = [0]
        self.fft_frames = [[0]]
        self.mfcc = []

        self.view = 2
        self.global_offset = 0
        self.clean_screen = True

    def record(self, data):

        amplitude = audioop.rms(data, 2)
        fft_data = np.abs(np.fft.rfft(data))
        
        self.frames.append(data)
        self.amp_frames.append(amplitude)
        self.fft_frames.append(fft_data)
        self.event_recorder.event_check(data, amplitude, fft_data)

    def get_frames(self):
        
        return self.frames

    def get_fft_frames(self):

        if self.fft_frames==[[0]]:
            i=0
            while i+self.CHUNK < len(self.get_frames()):
                data = self.frames[i: i+self.CHUNK]
                signal_fft_data = np.abs(np.fft.rfft(data))
                self.fft_frames.append(signal_fft_data)
                i+=self.CHUNK
            last_frame = self.frames[i-self.CHUNK:]
            self.fft_frames.append(np.abs(np.fft.rfft(last_frame)))
        
        return self.fft_frames

    def get_amp_frames(self):

        if self.amp_frames==[0]:
            i=0
            while i+self.CHUNK < len(self.get_frames()):
                data = self.frames[i: i+self.CHUNK]
                amplitude = audioop.rms(data, 2)
                self.amp_frames.append(amplitude)
                i+=self.CHUNK
            last_frame = self.frames[i-self.CHUNK:]
            self.amp_frames.append(audioop.rms(last_frame, 2))
        
        return self.amp_frames

    def get_mfcc(self):
        
        if not self.mfcc:
            self.mfcc = mfcc(np.array(self.get_fft_frames()), samplerate = self.RATE, numcep = 13)
        return self.mfcc

    def change_view(self, view):

        self.view = view
        self.clean_screen = True

    def clean_screen_check(self, screen):

        if self.clean_screen:
            screen.fill([255,255,255])
            self.clean_screen = False
            return False
        return True

    def draw_last(self, screen):
        
        self.clean_screen_check(screen)

        if self.view == 1:
            self.draw_fft_frame(screen, self.fft_frames[-1])
        elif self.view == 2:
            line_width = 1
            offset = (len(self.fft_frames)-1)*line_width
            if offset % screen.get_width() == 0:
                screen.fill([255,255,255])
            self.draw_spectrum(screen, self.fft_frames[-1], offset, line_width)
            self.draw_amplitude(screen, self.amp_frames[-1], offset, line_width)
        elif self.view == 3:
            self.draw_events(screen)

    def draw_all(self, screen):

        if self.clean_screen_check(screen):
            return None

        if self.view == 2:
            line_width = 1
            for num, frame in enumerate(self.get_fft_frames()):
                offset = num*line_width
                offset+=self.global_offset
                if 0<offset<screen.get_width():
                    self.draw_spectrum(screen, frame, offset, line_width)

            for num, frame in enumerate(self.get_amp_frames()):
                offset = num*line_width
                offset+=self.global_offset
                if 0<offset<screen.get_width():
                    self.draw_amplitude(screen, frame, offset, line_width)

    def draw_amplitude(self, screen, frame, offset=0, line_width=1):

        h = screen.get_height()
        w = screen.get_width()
        
        amplitude = frame/(h/8)
        offset = offset % w

        pygame.draw.rect(screen, [0,0,0], (offset, h-amplitude, line_width, amplitude))

    def draw_fft_frame(self, screen, frame):

        screen.fill([255,255,255])
        
        h = screen.get_height()
        w = screen.get_width()

        for num in range(len(self.sample_freq)-1):
            
            freq_pos = np.interp(self.sample_freq[num], [0, self.RATE/2], [0, w])
            pygame.draw.line(screen, [0,0,0], (freq_pos, h), (freq_pos, h-frame[num]/5000))

    def draw_spectrum(self, screen, frame, offset=0, line_width=1):
        
        h = screen.get_height()
        w = screen.get_width()
        
        freq_step = h//len(frame)
        offset = offset % w

        for freq, freq_amp in enumerate(frame):
            color = np.interp(freq_amp, [0, 500000], [255, 0])
            pygame.draw.rect(screen, [color,color,color], (offset, freq*freq_step, line_width, freq_step))

    def draw_events(self, screen):
        
        for num, event in enumerate(self.event_recorder.event_fft_data_list):
            offset = num * 30
            line_width = 1
            for data_chunk in event:
                self.draw_spectrum(screen, data_chunk, offset, line_width)
                offset+=line_width
        
        for num, event in enumerate(self.event_recorder.event_amp_list):
            offset = num * 30
            line_width = 1
            for amp_chunk in event:
                self.draw_amplitude(screen, amp_chunk, offset, line_width)
                offset+=line_width
