from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

from audio import AudioPlayer, SignalType, Task


class Classifier():
    
    def __init__(self):
        
        self.shots = [AudioPlayer(Task.PLAY, SignalType.SHOT, 'test/shot_1.wav'),
                      AudioPlayer(Task.PLAY, SignalType.SHOT, 'test/shot_2.wav'),
                      AudioPlayer(Task.PLAY, SignalType.SHOT, 'test/shot_3.wav')]
        self.reloads = [AudioPlayer(Task.PLAY, SignalType.RELOAD, 'test/reload_1.wav'),
                        AudioPlayer(Task.PLAY, SignalType.RELOAD, 'test/reload_2.wav'),
                        AudioPlayer(Task.PLAY, SignalType.RELOAD, 'test/reload_3.wav'),
                        AudioPlayer(Task.PLAY, SignalType.RELOAD, 'test/reload_4.wav')]
        self.trainng_set = [self.reloads, self.shots]
        self.test_set = []

    def get_dtw_distance(self, s1, s2):

        distance, path = fastdtw(s1.frames_handler.get_amp_frames(), s2.frames_handler.get_amp_frames(), dist=euclidean)
        return distance

    def dtw_test(self):

        v = AudioPlayer(Task.PLAY, SignalType.RANDOM, 'violin.wav')

        print("violin-violin: {}".format(self.get_dtw_distance(v, v)))

        print('#########################')
        
        for num, shot in enumerate(self.shots):
            print("violin-shot{}: {}".format(num+1, self.get_dtw_distance(v, shot)))

        print('#########################')

        for num, reload in enumerate(self.reloads):
            print("violin-reload{}: {}".format(num+1, self.get_dtw_distance(v, reload)))

        print('#########################')

        for num_s, shot in enumerate(self.shots):
            for num_r, reload in enumerate(self.reloads):
                print("shot{}-reload{}: {}".format(num_s+1, num_r+1 ,self.get_dtw_distance(shot, reload)))

        print('#########################')

        print("shot1-shot2: {}".format(self.get_dtw_distance(self.shots[0], self.shots[1])))
        print("shot2-shot3: {}".format(self.get_dtw_distance(self.shots[1], self.shots[2])))
        print("shot1-shot3: {}".format(self.get_dtw_distance(self.shots[0], self.shots[2])))

        print('#########################')

        print("reload1-reload2: {}".format(self.get_dtw_distance(self.reloads[0], self.reloads[1])))
        print("reload2-reload3: {}".format(self.get_dtw_distance(self.reloads[1], self.reloads[2])))
        print("reload1-reload3: {}".format(self.get_dtw_distance(self.reloads[0], self.reloads[2])))

    def classify_test(self):
        pass

    def classify(self, signal):
        shot_distance = 0
        reload_distance = 0

        for shot in self.shots:
            shot_distance+=self.get_dtw_distance(signal, shot)
        shot_distance/=len(self.shots)

        for reload in self.reloads:
            reload_distance+=self.get_dtw_distance(signal, reload)
        reload_distance/=len(self.reloads)

        if shot_distance<reload_distance:
            print('SHOT')
        else:
            print('RELOAD')
        print('SHOT DISTANCE: {}, RELOAD DISTANCE: {}'.format(shot_distance, reload_distance))
